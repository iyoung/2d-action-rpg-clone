#include "Effect.h"


Effect::Effect(float iX, float iY)
{
	x=iX;
	y=iY;
	dx=0;
	dy=0;
	inuse = true;
}
float Effect::getX()
{
	return x;
}
float Effect::getY()
{
	return y;
}
void Effect::setX(float newx)
{
	x=newx;
}
void Effect::setY(float newy)
{
	y=newy;
}
void Effect::setradius(float irad)
{
	radius = irad;
}
float Effect::getradius()
{
	return radius;
}
float Effect::getDX()
{
	return dx;
}
float Effect::getDY()
{
	return dy;
}
void Effect::setDX(float idx)
{
	dx=idx;
}
void Effect::setDY(float idy)
{
	dy = idy;
}
void Effect::portto(float newX, float newY)
{
	x=newX;
	y=newY;
}
void Effect::move()
{
	x+=dx;
	y+=dy;
}
void Effect::moveto(float idx, float idy)
{
	x+=idx;
	y+=idy;
}
bool Effect::isinuse()
{
	return inuse;
}
void Effect::setinuse(bool setter)
{
	inuse = setter;
}
Effect::~Effect(void)
{
}
