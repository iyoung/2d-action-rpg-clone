#pragma once
#include "effect.h"



class Nova :
	public Effect
{
public:
	Nova(float iX, float iY, float iRadius,float iRed, float iGreen, float iBlue);
	virtual void draw();
	~Nova(void);
private:
	float maxradius;
	float colour[4];
	bool effect[7];
	bool unlocked;
};

