#pragma once
#include "mobile.h"
#include "Effect.h"


class Character :
	public Mobile
{
public:
	Character(float iX, float iY);
	EffectType geteffect(int selector);
	virtual void draw();
	void movetoclick();
	~Character(void);
private:
	float length;
	float width;
	Effect* effect;
};

