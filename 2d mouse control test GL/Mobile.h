#pragma once
#include "Vertex.h"
#include <iostream>
#include <gl/glut.h>
#include "Effect.h"
#include <cmath>
//this class needs code to increment and get hitpoints.
class Mobile
{
public:
	Mobile(float iX, float iY);
	virtual void draw()=0;
	void setlevel(int setter);
	int getlevel();
	void addlevel(int adder);
	void apply_effect(EffectType effect);
	void seteffects(int mod);
	void port_to(int iX, int iY);
	void move_to(int iX, int iY);
	void moveto(float idx, float idy);
	void move();
	void stop();
	void set_speed(float mod);
	void set_vx(float iVx);
	void set_vy(float iVy);
	float getx();
	float gety();
	void setx(float setter);
	void sety(float setter); 
	float get_vx();
	float get_vy();
	float get_speed();
	float getradius();
	float getdistance();
	void setdistance();
	bool ismoving();
	void setmoving(bool setter);
	void dectimer(float mod);
	float getdtimer();
	void setdtimer(float setter);
	float getdestx();
	float getdesty();
	void setdestx(float setter);
	void setdesty(float setter);
	bool hasdest();
	~Mobile(void);
private:
	float x;
	float y;
	float speed;
	float vx;
	float vy;
	float distance;
	int level;
	float radius;
	bool moving;
	int hitpoints;
	float dtimer;
	Vertex *destination;

};

