#include <GL/glut.h>
#include <iostream>
#include "Character.h"
#include "Indicator.h"
#include "Effect.h"
#include "Fireball.h"
#include "Nova.h"
#include "EffectPool.h"
#include "Bitmap.h"
#include "Monster.h"
#include "Map.h"
using namespace std;


const int MAXFX = 50;
bool runbutton=false;
bool leftbutton=false;
bool shiftbutton = false;
bool ctrlbutton = false;
bool rightbutton = false;
bool buttonpress = false;
bool leftclick = false;
bool rightclick = false;
bool button1 = false;
bool button2 =false;
unsigned char pkeys[5]={'1','2','3','4','5'};
int abilitynum = 0;
int mousex;
int mousey;
bool keys[256]={false};
unsigned char pressedkeys[256]={NULL};
const int MAXPLAYERS=1;
Character *players[MAXPLAYERS] = {NULL};
Monster *monster = NULL;
Bitmap *cursor = NULL;
EffectPool *novas = NULL;
Effect *effect = NULL;
Map *map1;

void init()
{
	map1 = new Map("bg1.bmp", true,0,0);
	glutSetCursor(GLUT_CURSOR_NONE);
	cursor = new Bitmap("Mouse.bmp", true);
	//cursor->setOrientation(ROT_90);
	cursor->setOrientation(MIRROR_Y);
	novas = new EffectPool(MAXFX);
	players[0] = new Character(400.0f,300.0f);
	players[0]->setlevel(4);
	monster = new Monster(10.0f,10.0f);
	monster->setlevel(3);
	mousex=400;
	mousey=300;
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}
// REGION Output FUNCTIONS
void display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	map1->drawmap();
	novas->draweffects();
	for (int i =0; i<MAXPLAYERS; i++)
	{
		players[i]->draw();
	}
	
	monster->draw();
	cursor->drawAt(mousex,mousey);
	glutSwapBuffers();
}
//END REGION

//REGION PROCESS FUNCTIONS
void processkeyboard()
{
	//processes keyboard buffer states

	//processes keypresses
	//if 'r' key is pressed...
	if (keys['r'] && runbutton)
	{
		if (players[0]->get_speed() != 10)
		{
			players[0]->set_speed(10);
		}
	}
	if (!keys['r'] && runbutton)
	{
		if(players[0]->get_speed() != 5)
		{
			players[0]->set_speed(5);
		}
		runbutton=false;
	}
	//processes key releases
	//if ability key 1 was pressed
	if(button1 && !keys[pkeys[0]])
	{
		button1 = false;
		players[0]->stop();
		novas->addeffect(players[0]->geteffect(1), players[0]->getx(),players[0]->gety(),mousex,mousey);
	}
	//if ability key 2 was pressed
	if(button2 && !keys[pkeys[1]])
	{
		button2 = false;
		players[0]->stop();
		novas->addeffect(players[0]->geteffect(2), players[0]->getx(),players[0]->gety(),mousex,mousey);
	}
}
void processmouse()
{
	//if a button is clicked
	if(rightbutton && !buttonpress)
	{
		rightclick = true;
		buttonpress = true;
	}
	if(leftbutton && !buttonpress)
	{
		leftclick = true;
		buttonpress = true;
	}
	if(leftbutton && buttonpress)
	{
		if(!shiftbutton)
		{
			players[0]->move_to(mousex,mousey);
		}
	}
	//if a button was clicked
	if (buttonpress && (!leftbutton && !rightbutton))
	{
		buttonpress = false;
		// the left button was clicked
		if (leftclick)
		{
			if (!shiftbutton)
			{
				cout<<mousex<<" "<<mousey<<endl;
				players[0]->move_to(mousex,mousey);
				novas->addeffect(INDICATOR, players[0]->getx(), players[0]->gety(), mousex, mousey);
			}
			else
			{
				players[0]->stop();
				novas->addeffect(players[0]->geteffect(1), players[0]->getx(), players[0]->gety(), mousex, mousey);
			}
			leftclick = false;
		}
		//the right button was clicked
		if(rightclick)
		{
			players[0]->stop();
			novas->addeffect(players[0]->geteffect(2),players[0]->getx(), players[0]->gety(), mousex, mousey);
			rightclick = false;
		}
	}


}
bool lessthan(float num1, float num2)
{
	if (num1 < num2)
	{
		return true;
	}
	else return false;
}
bool collisionmonster(Monster *monster, Effect *effect)
{
	if(monster->distance(effect) <= monster->getradius())
	{
		return true;
	}
	else
	{
		return false;
	}
}
void collisionmanager()
{
	
	//create a new pointer to an array
	//make that pointer = returned pointer.
	if(novas->geteffects() != NULL)
	{
		Effect **effect = NULL;
		effect = novas->geteffects();//this is causing a memory exception
		for (int i = 0; i < MAXFX ;i++)
		{
			//if effect is not null, check if it is on screen. if not, set not in use
			if (effect[i] != NULL)
			{
				float x = effect[i]->getX();
				float y = effect[i]->getY();
				float radius = effect[i]->getradius();

				if (lessthan(x+radius, 0) || lessthan(800, x-radius) || lessthan(y+radius, 0) || lessthan(600, y-radius))
				{
						effect[i]->setinuse(false);
				}
				//checking collision with stuff
				else
				{
					if(collisionmonster(monster,effect[i]))
					{
						effect[i]->setinuse(false);
						monster->sethostile(true);
						monster->port_to(monster->getx()+effect[i]->getDX()/3,monster->gety()+effect[i]->getDY()/3);
					}
				}
			}
		}
	}
	//check if monster is on screen
	float x=monster->getx();
	float y=monster->gety();
	float radius = monster->getradius();
	if (!lessthan(x+radius, 0) && !lessthan(800, x-radius) && !lessthan(y+radius, 0) && !lessthan(600, y-radius))
	{
		if(!monster->ishostile())
		{
			monster->herolevelcheck(players[0]->getlevel());
			if(monster->aggroradius() >= monster->distance(players[0]))
			{
				monster->sethostile(true);
			}
		}
	}
	else
	{
		if(monster->ishostile())
		{
			monster->sethostile(false);
		}
	}
}
void update()
{
		processkeyboard();
		processmouse();
		if (players[0]->ismoving())
		{
			players[0]->movetoclick();
			float dx = -(players[0]->get_vx());
			float dy = -(players[0]->get_vy());
			map1->setx(map1->getx()+dx);
			map1->sety(map1->gety()+dy);
			novas->moveeffects(dx,dy);
			monster->setx(monster->getx()+dx);
			monster->sety(monster->gety()+dy);
		}
		collisionmanager();
		if(monster->ishostile())
		{
			monster->settarget(players);
			monster->seek();
			monster->move();
		}
		glutPostRedisplay();
}
void timer(int animating)
{
	update();
	// timer function to be called every 20 milliseconds (50fps Max)
	display();
	glutTimerFunc(20, timer, animating);
}
//END REGION

// Region INPUT FUNCTIONS

void mousemotion(int x, int y)
{
	mousex=x;
	mousey=y;
}
void mouse(int button, int state, int x, int y)
{
	//handling code for when a mouse button is pressed.
	switch (button)
		{
			case GLUT_RIGHT_BUTTON:
			{
				rightbutton = (state==GLUT_DOWN);
				mousex=x;
				mousey=y;
			}
			break;
			case GLUT_LEFT_BUTTON:
			{
				leftbutton = (state==GLUT_DOWN);
				mousex=x;
				mousey=y;
				if(glutGetModifiers() == GLUT_ACTIVE_SHIFT)
				{
					shiftbutton = true;
				}
				if(glutGetModifiers()!= GLUT_ACTIVE_SHIFT)
				{
					shiftbutton = false;
				}
				
				
			}
			break;
			case GLUT_MIDDLE_BUTTON:
				{}
				break;
		}
}
void keyboard(unsigned char key, int x, int y)
{
	//handling code for when a key is pressed
	if(keys[key] == false)
	{
		std::cout << key << " key pressed" << std::endl;
		pressedkeys[key] = key;
		keys[key] = true;
		if (key == pkeys[0])
		{
			button1 = true;
		}
		if (key == pkeys[1])
		{
			button2 = true;
		}
		if (key == 'r')
		{
			runbutton=true;
		}

	}
	
	
}
void keyboardup(unsigned char key, int x, int y)
{	//handling code for when a key is released
		
		std::cout << key << " key released" << std::endl;
		if (keys[key])
		{
			keys[key]=false;
		}
}
void motion(int x, int y)
{
	// Update the mouse coordinates
	mousex = x; 
	mousey = y;

}
void special_keys(int key, int x, int y)
{
	//special key press handling
}
void special_keysup(int key, int x, int y)
{
	//special key release handling
}
///END REGION
void cleanup()
{
	for (int i = 0;i<MAXPLAYERS;i++)
	{
		if (players[i])
		{
			delete players[i];
		}
		delete players;
	}
	delete monster;
	novas->cleanup();
	delete novas;
	delete cursor;
	delete effect;
}
int main(int argc, char** argv)
{	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100,100);
	glutInitWindowSize(800,600);
	glutCreateWindow("mouse control test");
	gluOrtho2D(0, 800, 600, 0);
	glClearColor(0.0f, 0.2f, 0.0f, 1.0f);
	init();
	glutDisplayFunc(display);
	glutMouseFunc(mouse);
	glutKeyboardFunc(keyboard);
	glutKeyboardUpFunc(keyboardup);
	glutSpecialFunc(special_keys);
	glutSpecialUpFunc(special_keysup);
	glutMotionFunc(motion);
	glutPassiveMotionFunc(mousemotion);
	glutTimerFunc(0, timer, 0);
	glutMainLoop();
	cleanup();
	return 0;
}