#pragma once
#include "mobile.h"
#include "Character.h"

class Monster :
	public Mobile
{
public:
	Monster(float iX, float iY);
	~Monster(void);
	virtual void draw();
	void settarget(Character *targets[]);
	void seek();
	float aggroradius();
	void herolevelcheck(int level);
	void sethostile(bool setter);
	bool ishostile();
	float distance(Character *target);
	float distance(Effect *effect);
private:
	Character *seektarget;
	float aggro;
	bool hostile;
	static const int baseaggro = 300;
};

