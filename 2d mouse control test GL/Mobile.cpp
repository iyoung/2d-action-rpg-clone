#include "Mobile.h"

Mobile::Mobile(float iX, float iY)
{
	x=iX;
	y=iY;
	destination = NULL;
	speed = 5;
	distance = 0;
	moving = false;
	radius = 20;
	vx=0;
	vy=0;
	hitpoints = 100+(10*level);
}
void Mobile::apply_effect(EffectType effect)
{
	switch (effect){
	case FIREBALL:
		{

		}
		break;
	case 2:
		{}
		break;
	case 3:
		{}
		break;
	case 4:
		{}
		break;
	case 5:
		{}
		break;
	case 6:
		{}
		break;
	}

}
void Mobile::seteffects(int mod)
{
	
}
void Mobile::setlevel(int setter)
{
	level = setter;
}
int Mobile::getlevel()
{
	return level;
}
void Mobile::addlevel(int adder)
{
	level+=adder;
}
void Mobile::port_to(int iX, int iY)
{
	x = iX;
	y = iY;
}
void Mobile::move_to(int iX, int iY)
{
	vx=0;
	vy=0;
	if (!destination)
	{
		delete destination;
		destination = new Vertex(iX,iY);
	}
	else
	{
		destination->setX(iX);
		destination->setY(iY);
	}
	//get relative co-ordinate differences
	float dX = destination->X()-x;
	float dY = destination->Y()-y;
	//get distance between reference point and destination
	distance = sqrt((dX*dX)+(dY*dY));
	float ratioX = dX/distance;
	float ratioY = dY/distance;
	vx = speed*ratioX;
	vy = speed*ratioY;
	if (distance != 0)
	{
		moving = true;
	}
	dtimer=distance;
}
void Mobile::move()
{
	if (!moving)
	{
		return;
	}
	float dX = destination->X()-x;
	float dY = destination->Y()-y;
	float ratioX = dX/distance;
	float ratioY = dY/distance;
	vx = speed*ratioX;
	vy = speed*ratioY;
	distance = sqrt((dX*dX)+(dY*dY));
	if (distance>speed)
	{
			x+=vx;
			y+=vy;
	}
	else
	{
			x=destination->X();
			y=destination->Y();
			vx=0;
			vy=0;
			moving = false;
	}
}
void Mobile::moveto(float idx, float idy)
{
	x+=idy;
	y+=idy;
}
void Mobile::stop()
{
	vx=0;
	vy=0;
	moving = false;
}
float Mobile::get_speed()
{
	return speed;
}
void Mobile::set_speed(float mod)
{
	speed = mod;
	if (destination)
	{
		float dX = destination->X()-x;
		float dY = destination->Y()-y;
		float ratioX = dX/distance;
		float ratioY = dY/distance;
		vx = speed*ratioX;
		vy = speed*ratioY;
	}
}
void Mobile::set_vx(float iVx)
{
	vx = iVx;
}
void Mobile::set_vy(float iVy)
{
	vy = iVy;
}
float Mobile::get_vx()
{
	return vx;
}
float Mobile::get_vy()
{
	return vy;
}
float Mobile::getx()
{
	return x;
}
float Mobile::gety()
{
	return y;
}
void Mobile::setx(float setter)
{
	x = setter;
}
void Mobile::sety(float setter)
{
	y = setter;
}
float Mobile::getradius()
{
	return radius;
}
bool Mobile::ismoving()
{
	return moving;
}
float Mobile::getdistance()
{
	return distance;
}
void Mobile::setdistance()
{
	float dX = getdestx()-getx();
	float dY = getdesty()-gety();
	distance = sqrt((dX*dX)+(dY*dY));
}
float Mobile::getdtimer()
{
	return dtimer;
}
void Mobile::setdtimer(float setter)
{
	dtimer = setter;
}
void Mobile::setmoving(bool setter)
{
	if(ismoving()!=setter)
	{
		moving = setter;
	}
}
void Mobile::dectimer(float mod)
{
	dtimer-=mod;
}
float Mobile::getdestx()
{
	return destination->X();
}
float Mobile::getdesty()
{
	return destination->Y();
}
void Mobile::setdestx(float setter)
{
	destination->setX(setter);
}
void Mobile::setdesty(float setter)
{
	destination->setY(setter);
}
bool Mobile::hasdest()
{
	if(destination)
	{
		return true;
	}
	else
	{
		return false;
	}
}
Mobile::~Mobile(void)
{
}
