#include "Indicator.h"


Indicator::Indicator(float iX, float iY): Effect(iX,iY)
{
	setradius(0);
	opacity = 1;
	setinuse(true);
	setType(INDICATOR);
}
void Indicator::draw()
{
	if(isinuse())
	{
		setradius(1+getradius());
		opacity-=0.05f;
		const int NPOINTS=25;
		const float TWOPI=2*3.1415927f;
		const float STEP=TWOPI/NPOINTS;
		
			glBegin(GL_LINE_LOOP);
			glColor4f(1,1.0,0.75,opacity);
				for (float angle=0;angle<TWOPI;angle+=STEP)
					{
						glVertex2f(getX()+getradius()*cos(angle), getY()+getradius()*sin(angle));
					}
			glEnd();
		
		if(getradius()>20)
		{
			setinuse(false);
			setradius(0);
			opacity=1.0f;
		}
	}
}
Indicator::~Indicator(void)
{
}
