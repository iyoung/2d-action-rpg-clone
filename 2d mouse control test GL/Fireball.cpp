#include "Fireball.h"


Fireball::Fireball(float iX, float iY, float dX, float dY, float ispeed):Effect(iX, iY)
{
	setradius(5.0f);
	speed = ispeed;
	collided = false;
	float diffx = dX-getX();
	float diffy = dY-getY();
	float distance =  sqrt(diffx*diffx + diffy*diffy);
	float vxratio = diffx/distance;
	float vyratio = diffy/distance;
	setDX(speed*vxratio);
	setDY(speed*vyratio);
	float tx=getX();
	float ty=getY();
	setX(tx+getDX());
	setY(ty+getDY());
	setType(FIREBALL);
}
void Fireball::draw()
{
	const int NPOINTS=25;
	const float TWOPI=2.0f*3.1415927f;
	const float STEP=TWOPI/NPOINTS;
	if(isinuse())
	{
		if(!collided)
		{
			float ang = 180.0f*atan2(getDY(), getDX())/3.14159268f;
			glPushMatrix();
			glTranslatef(getX(), getY(), 0);
			glRotatef(ang, 0, 0, 1);
			glTranslatef(-getX(), -getY(), 0);
			glBegin(GL_POLYGON);
			glColor3f(1,0.25,0.25);
			for (float angle=0;angle<TWOPI;angle+=STEP)
				{
					glVertex2f(getX()+getradius()*cos(angle), getY()+getradius()*sin(angle));
				}
			glEnd();
			glBegin(GL_POLYGON);
			glColor3f(1.0f,0.2f,0.2f);
				glVertex2f(getX(),getY()+getradius());
				glVertex2f(getX()-(2*getradius()),getY());
				glVertex2f(getX(),getY()-getradius());
			glEnd();
			glPopMatrix();
			move();
		}
		if(collided)
		{
			setradius(getradius()+2);
			glBegin(GL_POLYGON);
			glColor3f(1,0.25,0.25);
			for (float angle=0;angle<TWOPI;angle+=STEP)
				{
					glVertex2f(getX()+getradius()*cos(angle), getY()+getradius()*sin(angle));
				}
			glEnd();
			glBegin(GL_POLYGON);

			if (getradius() >= 20)
			{
				setDX(0);
				setDY(0);
				setinuse(false);
			}
		}
	}
}
Fireball::~Fireball(void)
{
}
