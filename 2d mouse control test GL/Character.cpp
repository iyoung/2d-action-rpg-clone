#include "Character.h"
#include <iostream>
using namespace std;


Character::Character(float iX, float iY):Mobile(iX, iY)
{
	length = 40.0f;
	width = 32.0f;
	effect = NULL;
}
EffectType Character::geteffect(int selector)
{
	switch (selector){
	case 1:
		{
			return FIREBALL;
		}
		break;
	case 2:
		{
			return NOVA;
		}
		break;
	}
}
void Character::draw()
{
	const int NPOINTS=25;
	const float TWOPI=2*3.1415927f;
	const float STEP=TWOPI/NPOINTS;
	glBegin(GL_POLYGON);
	glColor3f(0.5,1.0,0.5);
		for (float angle=0;angle<TWOPI;angle+=STEP)
			{
					
				glVertex2f(getx()+getradius()*cos(angle), gety()+getradius()*sin(angle));
			}
	glEnd();
}
void Character::movetoclick()
{
	if (hasdest())
	{
		float dX = getdestx()-getx();
		float dY = getdesty()-gety();
		float ratioX = dX/getdistance();
		float ratioY = dY/getdistance();
		set_vx(ratioX*get_speed());
		set_vy(ratioY*get_speed());

		if(getdistance() <=0)
		{
			stop();
			cout<<getdestx()<< ","<<getdesty()<<endl;
		}
		if(getdistance() > 0 && getdistance() < get_speed())
		{
			setdestx(getx());
			setdesty(gety());
			stop();
		}
		else
		{
			setdestx(getdestx()-get_vx());
			setdesty(getdesty()-get_vy());
			setdistance();
		}
	}
}
Character::~Character(void)
{
}
