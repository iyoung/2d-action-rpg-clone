#pragma once
#include "effect.h"
#include <gl/glut.h>
#include <cmath>

class Fireball :
	public Effect
{
public:
	Fireball(float iX, float iY, float dX, float dY, float ispeed);
	virtual void draw();
	float getspeed();
	void setcollide(bool collided);
	~Fireball(void);
private:
	float speed;
	bool collided;
};

