#include "Nova.h"


Nova::Nova(float iX, float iY, float iRadius,float iRed, float iGreen, float iBlue): Effect(iX,iY)
{
	maxradius = iRadius;
	setradius(0);
	colour[0] = iRed;
	colour[1] = iGreen;
	colour[2] = iBlue;
	colour[3] = 1.0;
	setinuse(true);
	setType(NOVA);
}
void Nova::draw()
{
	if(isinuse())
	{
		float step = 2;
		setradius(getradius()+step);
		colour[3] -= (1/(maxradius/step));
		const int NPOINTS=25;
		const float TWOPI=2*3.1415927f;
		const float STEP=TWOPI/NPOINTS;
		
			glBegin(GL_POLYGON);
			glColor4fv(colour);
				for (float angle=0;angle<TWOPI;angle+=STEP)
					{
						glVertex2f(getX()+getradius()*cos(angle), getY()+getradius()*sin(angle));
					}
			glEnd();
		
		if(getradius()>=maxradius)
		{
			setinuse(false);
			setradius(0);
			colour[3]=1;
		}
	}
}
Nova::~Nova(void)
{
}
