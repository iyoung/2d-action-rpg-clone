#pragma once
#include "effect.h"
class Indicator :
	public Effect
{
public:
	Indicator(float iX, float iY);
	virtual void draw();
	~Indicator(void);
private:
	float opacity;
};

