#include "EffectPool.h"

EffectPool::EffectPool(int setter)
{
	MAXFX=setter;
	fx = new Effect*[MAXFX];
	for (int i = 0; i < MAXFX; i++)
	{
		fx[i]=NULL;
	}
}
void EffectPool::draweffects()
{
	int i = 0;
	while (fx[i] && i<MAXFX)
	{
		if (fx[i]->isinuse())
		{
			fx[i]->draw();
		}
		i++;
	}
}
void EffectPool::addeffect(EffectType newtype, float x, float y, int mousex, int mousey)
{
	bool found = false;
	int i = 0;
	while (!found && i < MAXFX)
	{
		// if slot is empty
		if(!fx[i])
		{
			std::cout << "creating new effect" << std::endl;
			delete fx[i];
			switch (newtype)
			{
			case FIREBALL:
				{
					fx[i] = new Fireball(x,y,mousex,mousey,5.0f);
				}
				break;
			case NOVA:
				{
					fx[i] = new Nova(x,y,100.0f,0.2f,0.2f,1.0f);
				}
				break;
			case INDICATOR:
				{
					fx[i] = new Indicator(mousex,mousey);
				}
			break;
			}
			found = true;
		}
		//if slot is not empty
		if(fx[i] && !found)
		{
			//if slot is not in use
			if(!fx[i]->isinuse())
			{
				//if types match
				if(fx[i]->getType()==newtype)
				{
					std::cout << "recycling effect" << std::endl;
					switch (newtype)
					{
						case FIREBALL:
							{
								fx[i]->portto(x,y);
								float diffx = mousex-x;
								float diffy = mousey-y;
								float distance =  sqrt(diffx*diffx + diffy*diffy);
								float vxratio = diffx/distance;
								float vyratio = diffy/distance;
								fx[i]->setDX(5.0f*vxratio);
								fx[i]->setDY(5.0f*vyratio);
							}
							break;
						case NOVA:
							{
								fx[i]->portto(x,y);
							}
							break;
						case INDICATOR:
							{
								fx[i]->portto(mousex,mousey);
							}
						break;
						}
					fx[i]->setinuse(true);
					found = true;
				}
				//if types dont match
				if(!found && fx[i]->getType()!=newtype)
				{
					std::cout << "creating new effect" << std::endl;
					delete fx[i];
					switch (newtype)
					{
					case FIREBALL:
						{
							fx[i] = new Fireball(x,y,mousex,mousey,5.0f);
						}
						break;
					case NOVA:
						{
							fx[i] = new Nova(x,y,100.0f,0.2f,0.2f,1.0f);
						}
						break;
					case INDICATOR:
						{
							fx[i] = new Indicator(mousex,mousey);
						}
					break;
					}
					found = true;
				}
			}
			//if slot has active effect
			if(fx[i]->isinuse())
			{
				i++;
			}
		}
	}
}
Effect** EffectPool::geteffects()
{	
	//still to be tested
	//return an address of the array of pointers to Effects objects.
	if (fx[0])
	{
		return fx;
	}
	else
	{
		return NULL;
	}
}
void EffectPool::moveeffects(float idx, float idy)
{
	for (int i = 0; i<MAXFX;i++)
	{
		if(fx[i] && fx[i]->isinuse())
		{
			fx[i]->moveto(idx, idy);
		}
	}
}
void EffectPool::cleanup()
{
	for (int i =0;i<MAXFX;i++)
	{
		if(!fx[i])
		{
			delete fx[i];
		}
	}
	delete fx;
}
EffectPool::~EffectPool(void)
{
}
