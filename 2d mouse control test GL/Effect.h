#pragma once
#include <gl/glut.h>
#include <cmath>
//this is the basic class for all abilities
//this class can have the following  effects:
// 1. DAMAGE/HEAL Stat: reduce or increase a Stat (hit points, mana points, armor, hp regen, mp regen, speed, xp modifier, stamina, gold find%, magic find%, crit%, attack speed, attack power)
// 2. ROOT : DAMAGE SPEED to 0 for duration
// 3. STUN: DAMAGE SPEED && DAMAGE ATTACKSPEED to 0 for duration;
// 4. DOT: DAMAGE hp by amount every duration/period milliseconds
// 5. KNOCKBACK: MOVE affected monster by amount.
// 6. SNARE: DAMAGE speed and attack speed by amount% for duration.

enum EffectType{
	NONE,
	FIREBALL,
	NOVA,
	INDICATOR,
	AURA,
	FIRECONE,
	ICECONE,
	ICEBALL,
	FIRENOVA
};
enum EffectOnMobile{
	NOEFFECT,
	DAMAGE,
	HEAL,
	MOVE
};

class Effect
{
public:
	Effect(float iX, float iY);
	void setType(EffectType fx){myType = fx;}
	EffectType getType(){return myType;}
	virtual void draw()=0;
	float getX();
	float getY();
	void setX(float newx);
	void setY(float newy);
	float getDX();
	float getDY();
	void setradius(float iradius);
	float getradius();
	void setDX(float idx);
	void setDY(float idy);
	void portto(float newX, float newY);
	void moveto(float idx, float idy);
	void move();
	bool isinuse();
	void setinuse(bool setter);
	~Effect(void);
private:
	EffectType myType;
	float x;
	float y;
	float dx;
	float dy;
	bool inuse;
	float radius;
	float speed;
	int amount;
	float duration;
};

