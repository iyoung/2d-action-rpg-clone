#include "Vertex.h"

Vertex::Vertex(float inX, float inY)
{
	x = inX;
	y = inY;
}
void Vertex::setX(float inX)
{
	//modify value of x
	x = inX;
}
void Vertex::setY(float inY)
{
	//modify value of y
	y = inY;
}
float Vertex::X()
{
	return x;
}
float Vertex::Y()
{
	return y;
}
void Vertex::subX(float inX)
{
	//subtract input from x
	x-=inX;
}
void Vertex::subY(float inY)
{
	//subtract input from y
	y-=inY;
}
void Vertex::sumX(float multX)
{
	//multiply x by input
	x*=multX;
}
void Vertex::sumY(float multY)
{
	//multiply y by input
	y*=multY;
}
void Vertex::addX(float inX)
{
	//add input to x
	x+=inX;
}
void Vertex::addY(float inY)
{
	//add input to y
	y+=inY;
}
void Vertex::divX(float div)
{
	//divide x by input
	x/=div;
}
void Vertex::divY(float div)
{
	//divide y by input
	y/=div;
}
Vertex::~Vertex(void)
{
}
