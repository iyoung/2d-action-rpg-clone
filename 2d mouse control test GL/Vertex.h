#pragma once
class Vertex
{
public:
	
	Vertex(float inX, float inY);
	~Vertex(void);
	//mutator methods
	void setX(float inX);
	void setY(float inY);
	//general methods
	void subX(float inX);
	void subY(float inY);
	void sumX(float multX);
	void sumY(float multY);
	void addX(float inX);
	void addY(float inY);
	void divX(float div);
	void divY(float div);
	//accessor methods
	float X();
	float Y();
private:
	float x;
	float y;
};

