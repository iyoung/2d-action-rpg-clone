#include "Monster.h"
Monster::Monster(float iX, float iY):Mobile(iX, iY)
{
	seektarget = NULL;
	set_speed(5.0f);
	aggro = baseaggro;
	hostile = false;
}
float Monster::distance(Character *target)
{
	float diffx = target->getx()-getx();
	float diffy = target->gety()-gety();
	return sqrt(diffx*diffx+diffy*diffy);
}
float Monster::distance(Effect *effect)
{
	float diffx = effect->getX()-getx();
	float diffy = effect->getY()-gety();
	return sqrt(diffx*diffx+diffy*diffy);
}
void Monster::seek()
{
	if (seektarget)
	{
		move_to(seektarget->getx(), seektarget->gety());
	}
}
void Monster::settarget(Character *targets[])
{
	if(hostile)
	{
		bool sorting = true;
		int size = sizeof(targets);
		Character *temp = NULL;
		if(size/4 > 1)
		{
			for (int i = 0;i < size && sorting; i++)
			{
				sorting = false;
				for (int j = 0; j < size-1; j++)
				{
					if (distance(targets[j+1]) < distance(targets[j]))
					{
						temp = targets[j];
						targets[j] = targets[j+1];
						targets[j+1] = temp;
						sorting = true;
					}
				}
			}
			seektarget = targets[0];
		}
		else
		{
			seektarget = targets[0];
		}
	}
}
void Monster::draw()
{
	const int NPOINTS=25;
	const float TWOPI=2*3.1415927f;
	const float STEP=TWOPI/NPOINTS;
	glBegin(GL_POLYGON);
	glColor3f(1.0f,0.25f,0.25f);
		for (float angle=0;angle<TWOPI;angle+=STEP)
			{
				glVertex2f(getx()+getradius()*cos(angle), gety()+getradius()*sin(angle));
			}
	glEnd();
}
void Monster::sethostile(bool setter)
{
	hostile = setter;
}
bool Monster::ishostile()
{
	return hostile;
}
float Monster::aggroradius()
{
	return aggro;
}
void Monster::herolevelcheck(int level)
{
	aggro = (baseaggro*getlevel())/level;
}
Monster::~Monster(void)
{
}
