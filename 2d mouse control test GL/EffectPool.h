#pragma once
#include <gl/glut.h>
#include "Nova.h"
#include "Effect.h"
#include "Fireball.h"
#include "Indicator.h"
#include <iostream>
#include <cmath>

class EffectPool
{
public:
	EffectPool(int setter);
	void draweffects();
	void addeffect(EffectType newtype, float x, float y, int mousex, int mousey);
	Effect** geteffects();
	void moveeffects(float idx, float idy);
	void cleanup();
	~EffectPool(void);
private:
	int MAXFX;
	Effect **fx;

};

