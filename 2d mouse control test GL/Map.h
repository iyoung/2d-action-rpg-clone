#pragma once
#include <GL/glut.h>
#include "Bitmap.h"
class Map:
	public Bitmap
{
public:
	Map(const char fname[], bool transparency, float ix, float iy);
	~Map(void);
	void drawmap();
	float getx(){return x;}
	float gety(){return y;}
	void setx(float setter){x = setter;}
	void sety(float setter){y = setter;}

private:
	float x;
	float y;
};

